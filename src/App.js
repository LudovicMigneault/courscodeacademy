import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Yelp} from './util/Yelp.js';

import BusinessList from "./Components/BusinessList/BusinessList.js";
import SearchBar from "./Components/SearchBar/SearchBar.js";


class App extends Component {
  constructor(par){
super(par);
    
    this.state = {
      businesses: []
    };
    this.searchYelp= this.searchYelp.bind(this);
  }
  searchYelp(term, location, sortBy){
    var that = this;
    console.log("Searching Yelp with : " + term + " , " + location + " , " + sortBy);
    Yelp.search(term, location, sortBy)
    .then(bs=>{
      console.log(bs);
      that.setState({businesses:bs});
    });
  }

  render() {
    return (
      <div className="App">
  <h1>ravenous</h1>
  <SearchBar searchYelp={this.searchYelp} />
  <BusinessList businesses={this.state.businesses} />
</div>
    );
  }
}

export default App;


/* DEFAULT Render Return : 
return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Hello Codeacademy!</h1>
        </header>
        <p className="App-intro">
          Congratz! now running React. You can <code>clean up files</code> 
          and add in some functions to app s Ludo changé .
      <br/>
      Woohoo!
        </p>
      </div>
    ); */