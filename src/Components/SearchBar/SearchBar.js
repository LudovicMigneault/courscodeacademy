import React from "react";
import ReactDom from "react-dom";
import './SearchBar.css';

//Déplacer dans constructor?
const sortByOptions = {
    'Best Match': 'best_match',
    'Highest Rated': 'rating',
    'Most Reviewed': 'review_count'
  };


class SearchBar extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            term:"",
            location:"",
            sortBy:"best_match"
        };

        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleTermChange = this.handleTermChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }
getSortByClass(sortByOption){
    return this.state.sortBy == sortByOption ? "active" : "";
}
    handleSortByChange(sortByOption){
        this.setState({sortBy:sortByOption});
    }

handleTermChange(event){
    console.log(event.target.value);
    this.setState({term: event.target.value});
}
handleLocationChange(event){
    console.log(event.target.value);
    this.setState({location: event.target.value});
}

handleSearch(ev){
    this.props.searchYelp(this.state.term, this.state.location, this.state.sortBy);
    ev.preventDefault();
}
    renderSortByOptions(){
        return Object.keys(sortByOptions)
        .map(key=><li onClick={this.handleSortByChange.bind(this,sortByOptions[key])} className={this.getSortByClass(sortByOptions[key] )} key={key}>{sortByOptions[key]}</li>);
    }
    render(){
        return (
            <div className="SearchBar">
  <div className="SearchBar-sort-options">
    <ul>
      {this.renderSortByOptions()}
    </ul>
  </div>
  <div className="SearchBar-fields">
    <input onChange={this.handleTermChange} placeholder="Search Businesses" />
    <input onChange={this.handleLocationChange} placeholder="Where?" />
  </div>
  <div className="SearchBar-submit">
    <a onClick={this.handleSearch}>Let's Go</a>
  </div>
</div>
        );
    }
}





export default SearchBar;