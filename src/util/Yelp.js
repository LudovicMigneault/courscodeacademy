
const apiKey = "rsp-zrhhPr_PQgzFRpEmohJ9DNC6JmmbTkQXCMVF1vlZ9Jtn5xABC7t2mKT9h6xj2BCi8_ZmG_t-Kkiln6GOV-40PYTKaAuYO_uedkXV-A_8OeaHNnNc4pW_X1ylW3Yx";

const Yelp2 = {
search : function(term,location,sortBy){
    //"https://cors-anywhere.herokuapp.com/"+ 
    return fetch(`https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/search?term=${term}&location=${location}&sort_by=${sortBy}`,
    { 
        headers: {
          Authorization: `Bearer ${apiKey}` 
        }
      }).then(response => {
        return response.json();
      })
      .then(jsonResponse => {
          if(jsonResponse.businesses){
            return jsonResponse.businesses.map(bs =>
               ({
                   id: bs.id,
                   imageSrc: bs.image_url,
                   name: bs.name,
                   address: bs.address,
                   city:bs.city,
                   state:bs.state,
                   zipCode:bs.zipCode,
                   category:bs.category,
                   rating:bs.rating,
                   reviewCount:bs.reviewCount
               }));
        //jsonResponse.businesses.map(bs=>
        //     <Business business={bs} />);
          }
      });
}
};

export const Yelp = Yelp2;