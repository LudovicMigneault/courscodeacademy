import React from "react";
import ReactDOM from "react-dom";
import "./BusinessList.css";
import Business from '../Business/Business';

class BusinessList extends React.Component {
render(){
    var i=123;
return (
    <div className="BusinessList">
{this.props.businesses.map(bs=><Business key={bs.id} business={bs}/>)}
</div>
);
}
};


export default BusinessList;